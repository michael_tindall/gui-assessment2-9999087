﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SupportForm
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            var subjects = new List<String>();
            subjects.Add("Attendance");
            subjects.Add("Issues");
            subjects.Add("Staff");
            subjects.Add("Equipment");
            subjects.Add("Concerns");
            subjects.Add("Other");
            subjectBox.ItemsSource = subjects;
        }

        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            //var title = "";
            var from = toBox.Text;
            var subject = subjectBox.SelectedItem;
            var message = messageBox.Text;
            //System Windows.UI.Popups;
            //title.Title = "Confirm submission";
            //title.Commands.Add(new UICommand { Labal = "Submit", Id = 0 });
            //title.Commands.Add(new UICommand { Labal = "Cancel", Id = 1 });
            //title.Commands.Add(new UICommand { Labal = "Clear", Id = 2 });
            //var res = await title.ShowAsync();
            //if ((int)res.Id == 0)
            //{ }
        }
    }
}
